package io.renren.dubbo;

public interface CityDubboService {
	
	City findCityByName(String cityName);
}
