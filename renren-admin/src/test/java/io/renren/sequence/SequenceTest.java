package io.renren.sequence;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.renren.common.utils.sequence.Sequence;


/**
 *  雪花算法生成唯一的ID
 *  
 * @author wb-yc404201
 * @date 2018年5月24日 下午3:39:06
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SequenceTest {
	
	@Test
	public void testSequence() throws Exception {
		Sequence sequence = new Sequence(0, 0);
		System.out.println(sequence.nextId());
	}
	
	
	public static void main(String[] args) {
		Set<Long> set = new HashSet<Long>();
		final Sequence idWorker1 = new Sequence(0, 0);
		final Sequence idWorker2 = new Sequence(1, 0);
		Thread t1 = new Thread(new IdWorkThread(set, idWorker1));
		Thread t2 = new Thread(new IdWorkThread(set, idWorker2));
		t1.setDaemon(true);
		t2.setDaemon(true);
		t1.start();
		t2.start();
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	static class IdWorkThread implements Runnable {
		private Set<Long> set;
		private Sequence idWorker;

		public IdWorkThread(Set<Long> set, Sequence idWorker) {
			this.set = set;
			this.idWorker = idWorker;
		}

		@Override
		public void run() {
			while (true) {
				long id = idWorker.nextId();
				if (!set.add(id)) {
					System.out.println("duplicate:" + id);
				}
			}
		}
	}
	
	
}
