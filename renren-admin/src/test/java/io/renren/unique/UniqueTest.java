package io.renren.unique;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.renren.modules.unique.service.TIdentityService;

/**
 * 根据数据库生成唯一连续的id
 * 
 * @author yangcong
 * @date 2018年5月25日 下午3:26:03
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UniqueTest {
	@Autowired
	private TIdentityService service;
	
	@Test
	public void testUniqueId() throws Exception {
		Set<String> set = new HashSet<>();
		ExecutorService pool = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 10000; i++) {
			Future<String> future = pool.submit(new UniqueThread(service));
			String string = future.get();
			set.add(string);
		}
		Assert.assertEquals(10000, set.size());
		System.out.println(set.size());
		Thread.sleep(20000);
		pool.shutdown();
	}
	
}
/**
 *  每一次生成一个值
 *  
 * @author wb-yc404201
 * @date 2018年5月25日 上午10:12:48
 */
class UniqueThread implements Callable<String>{
	private TIdentityService service;
	UniqueThread(TIdentityService service){
		this.service = service;
	}
	@Override
	public String call() throws Exception {
		String id = null;
		try {
			id = service.nextId("OrderNo");
		} catch (Exception e) {
			
		}
		return id;
	}
	
}











