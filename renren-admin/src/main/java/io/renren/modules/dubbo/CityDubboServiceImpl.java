package io.renren.modules.dubbo;

import com.alibaba.dubbo.config.annotation.Service;
/**
 * 注册为dubbo服务
 * 
 * @author yangcong
 * @date 2018年5月30日 下午4:30:09
 */
@Service(version = "1.0.0")
public class CityDubboServiceImpl implements CityDubboService{

	@Override
	public City findCityByName(String cityName) {
		City city = new City();
		if ("云梦".equals(cityName)) {
			city.setName("云梦");
			city.setNum(1L);
		}
		return city;
	}

}
