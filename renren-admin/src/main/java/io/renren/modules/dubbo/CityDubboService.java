package io.renren.modules.dubbo;

public interface CityDubboService {
	
	City findCityByName(String cityName);
}
