package io.renren.modules.unique.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

/**
 *  实体类
 * @author yangcong
 * @date 2018年5月25日 下午3:15:01
 */
@TableName("t_identity")
public class TIdentityEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	private BigDecimal id;
	private String name;
	private String alias;
	private String rule;
	private Integer genType;
	private Integer noLength;
	private Integer initValue;
	private Integer curValue;
	private Integer step;
	private String curDate;
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getId() {
		return id;
	}
	/**
	 * 设置：名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：别名
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	/**
	 * 获取：别名
	 */
	public String getAlias() {
		return alias;
	}
	/**
	 * 设置：规则
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}
	/**
	 * 获取：规则
	 */
	public String getRule() {
		return rule;
	}
	/**
	 * 设置：每天生成
	 */
	public void setGenType(Integer genType) {
		this.genType = genType;
	}
	/**
	 * 获取：每天生成
	 */
	public Integer getGenType() {
		return genType;
	}
	/**
	 * 设置：流水号长度
	 */
	public void setNoLength(Integer noLength) {
		this.noLength = noLength;
	}
	/**
	 * 获取：流水号长度
	 */
	public Integer getNoLength() {
		return noLength;
	}
	/**
	 * 设置：初始值
	 */
	public void setInitValue(Integer initValue) {
		this.initValue = initValue;
	}
	/**
	 * 获取：初始值
	 */
	public Integer getInitValue() {
		return initValue;
	}
	/**
	 * 设置：当前值
	 */
	public void setCurValue(Integer curValue) {
		this.curValue = curValue;
	}
	/**
	 * 获取：当前值
	 */
	public Integer getCurValue() {
		return curValue;
	}
	/**
	 * 设置：步长
	 */
	public void setStep(Integer step) {
		this.step = step;
	}
	/**
	 * 获取：步长
	 */
	public Integer getStep() {
		return step;
	}
	/**
	 * 设置：
	 */
	public void setCurDate(String curDate) {
		this.curDate = curDate;
	}
	/**
	 * 获取：
	 */
	public String getCurDate() {
		return curDate;
	}
}
