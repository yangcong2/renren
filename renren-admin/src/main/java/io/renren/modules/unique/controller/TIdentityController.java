package io.renren.modules.unique.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import io.renren.common.utils.R;
import io.renren.modules.unique.service.TIdentityService;

@Controller
@RequestMapping("/unique")
public class TIdentityController {
	
	@Autowired
	private TIdentityService tIdentityService;
	
	@RequestMapping("index")
	@ResponseBody
	public R index() throws Exception{
		tIdentityService.testException(2);
		return R.ok();
	}
	
}
