package io.renren.modules.unique.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import io.renren.modules.unique.entity.TIdentityEntity;

/**
 *  DAO层
 * @author yangcong
 * @date 2018年5月25日 下午3:16:05
 */
public interface TIdentityDao extends BaseMapper<TIdentityEntity> {
	
	TIdentityEntity getByAlias(String alias);
	
	Long getMaxId(String column, String table);
	
	
	
}
