package io.renren.modules.unique.service;

import java.util.Map;

import com.baomidou.mybatisplus.service.IService;

import io.renren.common.exception.RRException;
import io.renren.common.utils.PageUtils;
import io.renren.modules.unique.entity.TIdentityEntity;

/**
 * 
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2018-05-24 16:25:24
 */
public interface TIdentityService extends IService<TIdentityEntity> {

    PageUtils queryPage(Map<String, Object> params);

    public String nextId(String alias) throws Exception;
	
	public Long getMaxId(String column, String table) throws Exception;

	void testException(int i) throws Exception;
}

